#include <stdlib.h>
#include <stdio.h>
#include <glut.h>

void init(void);
void display(void);
void keyboard(unsigned char key, int x, int y);
void reshape(int w, int h);

#define PRETO    0.0, 0.0, 0.0
#define AZUL     0.0, 0.0, 1.0
#define VERMELHO 1.0, 0.0, 0.0
#define AMARELO  1.0, 1.0, 0.0
#define VERDE    0.0, 1.0, 0.0
#define CYAN     1.0, 0.0, 1.0
#define LARANJA  0.8, 0.6, 0.1
#define ROSEO    0.7, 0.1, 0.6
#define CINZA    0.6, 0.6, 0.6
#define CINZA2   0.7, 0.7, 0.7
#define CINZA3   0.5, 0.5, 0.5
#define MARROM   0.43, 0.42, 0.34

GLubyte colors[] =
{
	200, 0, 0,      /* 0- Vermelho */
	255, 0, 0,      /* 1- Vermelho */
	255, 0, 0,      /* 2- Vermelho */
	200, 0, 0,      /* 3- Vermelho */
	0, 0, 200,      /* 4-  AZUL */
	0, 0, 255,      /* 5-  AZUL */
	0, 0, 255,      /* 6-  AZUL */
	0, 0, 200,      /* 7-  AZUL  */
	32, 32, 32,  /* 8-  CINZA  */   // Anel
	32, 32,  32,  /* 9-  CINZA2  */
	32, 32,  32,   /* 10- CINZA3 */
	32,   32,    32, /* 11  CINZA2  */
	32,  32,    32, /* 12- CINZA3 */
	32,  32,    32, /* 13  CINZA2  */
	32,  32,    32, /* 14- CINZA3 */
	32,  32,    32, /* 15  CINZA2  */
	32,  32,    32, /* 16- CINZA3 */ // Cabo
	84,80,62, /* 17  CINZA2  */
	84,80,62, /* 18- CINZA3 */
	84,80,62, /* 19  CINZA2  */
	133,127,103, /* 20- CINZA3 */
	133,127,103, /* 21  CINZA2  */
	133,127,103, /* 22- CINZA3 */
	133,127,103, /* 23  CINZA2  */
	84,80,62, /* 24- CINZA3 */
	84,80,62, /* 25  CINZA2  */
	84,80,62, /* 26- CINZA3 */
	84,80,62, /* 27  CINZA2  */
};


static GLfloat verticesLamina[96]{
  0.0,  1.5, 150.0, /* 0 */
  12.5 , 1.5, 150.0, /* 1 */
  20.0, -1.0, 150.0, /* 2 */
  0.0,  -1.0, 150.0, /* 3 */
  0.0,  1.5, 5.0,   /* 4 */
  12.5, 1.5, 20.0,  /* 5 */
  20.0, -1.0, 20.0,  /* 6 */
  0.0,  -1.0, 0.0,   /* 7 */

  -5.0,  5.0, 150.0, /*  8 */ // Começa o anel de divisão
  25.0, 5.0, 150.0,  /*  9 */
  25.0, -5.0, 150.0, /* 10 */
  -5.0, -5.0, 150.0, /* 11 */
  -5.0, 5.0, 155.0,  /* 12 */
  25.0, 5.0, 155.0,  /* 13 */
  25.0, -5.0, 155.0, /* 14 */
  -5.0, -5.0, 155.0, /* 15 */

  0.0,  2.5, 205.0, /* 16 * -- 0*/ //Começa o cabo 
  20.0, 2.5, 205.0, /* 17 * -- 1*/
  20.0, -2.0, 205.0,/* 18 * -- 2*/
  0.0,  -2.0, 205.0,/* 19 * -- 3*/
  0.0,  2.5, 155.0, /* 20 * -- 4*/
  18.0, 2.5, 155.0, /* 21 * -- 5*/
  18.0, -2.0, 155.0, /* 22 * -- 6*/
  0.0,  -2.0, 155.0,/* 23 * -- 7*/
  20.0, 2.5, 200.0, /* 24 -- 8*/
  20.0, -2.0, 200.0, /* 25 -- 9*/
  18.0, 2.5, 198.0, /* 26 -- 11*/
  18.0, -2.0, 198.0, /* 27 -- 10*/
};

static GLubyte LaminafrenteIndices[] = { 0,1,2,3 };
static GLubyte LaminatrasIndices[] = { 4,5,6,7 };
static GLubyte LaminaesquerdaIndices[] = { 0,4,7,3 };
static GLubyte LaminadireitaIndices[] = { 1,2,6,5 };
static GLubyte LaminatopoIndices[] = { 0,1,5,4 };
static GLubyte LaminafundoIndices[] = { 3,7,6,2 };

static GLubyte AnelfrenteIndices[] = { 8,9,10,11 };
static GLubyte AneltrasIndices[] = { 12,15,14,13 };
static GLubyte AnelesquerdaIndices[] = { 11,15,12,8 };
static GLubyte AneldireitaIndices[] = { 13,14,10,9 };
static GLubyte AneltopoIndices[] = { 12,13,9,8 };
static GLubyte AnelfundoIndices[] = { 14,15,11,10 };

static GLubyte CabofrenteIndices[] = { 16,19,18,17 };
static GLubyte CabotrasIndices[] = { 20,23,22,21 };
static GLubyte CaboesquerdaIndices[] = { 16,20,23,19 };
static GLubyte CabodireitaIndices[] = { 26,27,22,21 };
static GLubyte Cabodireita2Indices[] = { 24,25,27,26 };
static GLubyte Cabodireita3Indices[] = { 17,18,25,24 };
static GLubyte CabotopoIndices[] = { 16,17,24,26,21,20 };
static GLubyte CabofundoIndices[] = { 23,22,27,25,18,19 };


static float scale = 1.0;

static float translateX = 10.0;
static float translateY = 23;
static float translateZ = -50;



static int eixoy;
static int eixox = 90;
int largura, altura;

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(480, 640);
	glutInitWindowPosition(500, 200);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutReshapeFunc(reshape);
	glutMainLoop();
	return 0;
}

void init(void) {
	glClearColor(.75, 0.5, 1.0, 0.0);
	glOrtho(-100, 200, -230, 230, -250, 250);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}

void reshape(int w, int h) {
	glViewport(10, 10, (GLsizei)w, (GLsizei)h);
	largura = w;
	altura = h;
}

void display(void) {
	glPushMatrix();
	glScalef(scale, scale, scale);
	glRotatef((GLfloat)eixoy, 0.0, 1.0, 0.0);
	glRotatef((GLfloat)eixox, 1.0, 0.0, 0.0);
	glTranslatef(translateX, translateY, translateZ);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);


	glVertexPointer(3, GL_FLOAT, 0, verticesLamina);
	glColorPointer(3, GL_UNSIGNED_BYTE, 0, colors);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminafrenteIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminaesquerdaIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminatrasIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminadireitaIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminatopoIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, LaminafundoIndices);



	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AnelfrenteIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AnelesquerdaIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AneltrasIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AneldireitaIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AneltopoIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, AnelfundoIndices);



	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, CabofrenteIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, CabotrasIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, CaboesquerdaIndices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, Cabodireita2Indices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, Cabodireita3Indices);

	glDrawElements(GL_POLYGON, 4, GL_UNSIGNED_BYTE, CabodireitaIndices);

	glDrawElements(GL_POLYGON, 6, GL_UNSIGNED_BYTE, CabotopoIndices);

	glDrawElements(GL_POLYGON, 6, GL_UNSIGNED_BYTE, CabofundoIndices);



	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 27:
		exit(0);
		break;
	case 'a':
		eixoy = (eixoy + 5) % 360;
		glutPostRedisplay();
		break;
	case 'd':
		eixoy = (eixoy - 5) % 360;
		glutPostRedisplay();
		break;
	case 'i':
		translateY = translateY + 5;
		glutPostRedisplay();
		break;
	case 'j':
		translateX = translateX - 5;
		glutPostRedisplay();
		break;
	case 'l':
		translateX = translateX + 5;
		glutPostRedisplay();
		break;
	case 'k':
		translateY = translateY - 5;
		glutPostRedisplay();
		break;
	case 'z':
		if (scale < 3.49) {
			scale = scale + 0.1;
		}
		glutPostRedisplay();
		break;	
	case 'x':
		if (scale > 0.1) {
			scale = scale - 0.1;
		}
		glutPostRedisplay();
		break;
	case 's':
		eixox = (eixox - 5) % 360;
		glutPostRedisplay();
		break;
	case 'p':
		glLoadIdentity();
		gluPerspective(75.0, (GLfloat)largura / (GLfloat)altura * 2, 20.0, 550.0);
		gluLookAt(40, -100, -170, 0, -50, 0, 0, 1, 0);
		glutPostRedisplay();
		break;
	case 'o':
		glLoadIdentity();
		glOrtho(-230, 230, -230, 230, -250, 250);
		glutPostRedisplay();
		break;
	}
}
